<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::redirect('register', 'login', 301);


Route::get('/home', 'HomeController@index')->name('home');


# accounts routes

Route::get('/accounts', 'AccountController@get_accounts')->name('accounts');
Route::get('/accounts/create', 'AccountController@create')->name('add-new-account');
Route::post('/accounts/store', 'AccountController@store')->name('create-account');
Route::get('/account/{account}/edit', 'AccountController@edit')->name('account-edit');
Route::post('/account/{account}/update', 'AccountController@update')->name('update-account');
Route::get('/account/{account}/delete-confirm', 'AccountController@confirm_delete')->name('delete-account-confirm');
Route::get('/account/{account}/delete', 'AccountController@delete')->name('delete-account');


# account users routes

Route::get('/account/{account}/users', 'UserController@index')->name('account-users');
Route::get('/account/{account}/users/create', 'UserController@create')->name('add-new-user');
Route::post('/account/{account}/users/store', 'UserController@store')->name('store-user');

Route::get('/users/{user}/edit', 'UserController@edit')->name('user-edit');
Route::post('/users/{user}/update', 'UserController@update')->name('update-user');

Route::get('/users/{user}/confirm-delete', 'UserController@confirm_delete')->name('delete-user-confirm');
Route::get('/users/{user}/delete', 'UserController@delete')->name('delete-user');



# account organizers

Route::get('/account/{account}/organizers', 'OrganizerController@index')->name('account-organizers');
Route::get('/account/{account}/organizers/create', 'OrganizerController@create')->name('add-new-organizer');
Route::post('/account/{account}/organizers/store', 'OrganizerController@store')->name('store-organizer');

Route::get('/organizers/{organizer}/edit', 'OrganizerController@edit')->name('organizer-edit');
Route::post('/organizers/{organizer}/update', 'OrganizerController@update')->name('update-organizer');

Route::get('/organizers/{organizer}/confirm-delete', 'OrganizerController@confirm_delete')->name('delete-organizer-confirm');
Route::get('/organizers/{organizer}/delete', 'OrganizerController@delete')->name('delete-organizer');

