<a class="btn btn-outline-primary" href="{{ route('account-edit',['account' => $id]) }}">edit</a>
<a href="{{ route('delete-account-confirm',$id) }}" class="btn btn-outline-danger">delete</a>
<a class="btn btn-outline-info" href="{{route('account-users',['account' => $id])}}">users</a>
<a class="btn btn-outline-info" href="{{route('account-organizers',['account' => $id])}}">organizers</a>