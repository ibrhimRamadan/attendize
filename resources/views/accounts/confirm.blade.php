@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Confirm Delete <span style="color: #1d68a7">{{ $account->name }}</span> Account</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p> Are You Sure To Delete This Account ? </p>

                         <a href="{{ route('accounts') }}" class="btn btn-primary">Back</a> <a href="{{ route('delete-account',$account->id) }}" class="btn btn-danger">Delete</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
