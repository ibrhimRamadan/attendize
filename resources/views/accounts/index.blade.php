@extends('layouts.app')



@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">accounts</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <a class="btn btn-outline-primary" href="{{ route('add-new-account') }}"> add new account</a>

                            {!! $dataTable->table(['class' => 'table' ,'id' => 'accountsDataTable' , 'width' => '100%'],true) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




@section('scripts')
    {!! $dataTable->scripts() !!}
@endsection