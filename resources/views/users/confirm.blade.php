@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Confirm Delete User <span style="color: #1d68a7">{{ $user->first_name }}</span></div>

                    <div class="card-body">

                        <p> Are You Sure To Delete This User ? </p>

                         <a href="{{ route('account-users',$user->account_id) }}" class="btn btn-primary">Back</a> <a href="{{ route('delete-user',$user->id) }}" class="btn btn-danger">Delete</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
