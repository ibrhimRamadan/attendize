@extends('layouts.app')



@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">users of account <span style="color: #1d68a7"> {{ $account->name  }} </span></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <a class="btn btn-outline-primary" href="{{ route('add-new-user',$account->id) }}"> add new user</a>

                            {!! $dataTable->table(['class' => 'table' ,'id' => 'usersDataTable' , 'width' => '100%'],true) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




@section('scripts')
    {!! $dataTable->scripts() !!}
@endsection