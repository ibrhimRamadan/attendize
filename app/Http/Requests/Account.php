<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Account extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'first_name' => 'required',
            'last_name' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:mysql2.accounts,email',
            'phone' => 'required|unique:mysql2.users,phone',
            'address1' => 'required',
            'address2' => 'required',
            'city' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];


        return $rules;
    }
}
