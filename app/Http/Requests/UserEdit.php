<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return  [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'password' => 'confirmed',
            'password_confirmation' => '',
            'email' =>  'required|email|unique:mysql2.users,email,'.$this->user,


        ];
    }
}
