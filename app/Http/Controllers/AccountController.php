<?php

namespace App\Http\Controllers;


use App\Account;
use App\DataTables\AccountDataTable;
use App\Events\AccountCreated;
use App\Http\Requests\Account as Account_Request;
use App\Http\Requests\AccountEdit as AccountEdit_Request;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function get_accounts(AccountDataTable $dataTable)
    {

        return $dataTable->render('accounts.index');
    }

    public function create()
    {
        return view('accounts.create');
    }

    public function store(Account_Request $request)
    {
        $account = Account::create([
            'name' => $request['name'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'city' => $request['city'],
            'address1' => $request['address1'],
            'address2' => $request['address2'],
            'email' => $request['email'],
            'timezone_id' => 30,
            'currency_id' => 2,
        ]);


        $account->password = bcrypt($request['password']);
        $account->phone = $request['phone'];

        # associated user and organizer
        event(new AccountCreated($account));



        return redirect('/accounts')->with('success','added successfully');
    }


    public function edit($account)
    {

        $account = Account::find($account);
        if (!$account){
            return redirect('/accounts')->with('error','not found');
        }

        return view('accounts.edit', compact('account'));
    }


    public function update($account,AccountEdit_Request $request)
    {

        $account = Account::find($account);

        if ($account)
        {
            $account->update([
                'name' => $request['name'],
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'city' => $request['city'],
                'address1' => $request['address1'],
                'address2' => $request['address2'],
                'email' => $request['email'],
                'timezone_id' => 30,
                'currency_id' => 2,
            ]);
        }

        return redirect()->back()->with('status', 'updated successfully');
    }


    public function confirm_delete($account)
    {
        $account = Account::find($account);
        if ($account)
        {
            return view('accounts.confirm',compact('account'));
        }

        return 'no account found with this id';
    }


    public function delete($account)
    {
        $account = Account::find($account);
        if ($account)
        {

            //TODO:: check if user related with it and delete them

            $account->delete();
            return redirect('/accounts')->with('status', 'deleted successfully');;
        }

        return 'no account found with this id';
    }
}
