<?php

namespace App\Http\Controllers;

use App\Account;
use App\DataTables\UserDataTable;
use App\Http\Requests\User as UserRequest;
use App\Http\Requests\UserEdit;
use App\SuperUser;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index($account, UserDataTable $dataTable)
    {
        $account = Account::findOrFail($account);
        return $dataTable->with('account_id', $account->id)->render('users.index',compact('account'));
    }

    public function create($account)
    {

        $account = Account::findOrFail($account);
        return view('users.create',compact('account'));

    }

    public function store($account , UserRequest $request)
    {

        SuperUser::create([
            'first_name' => $request['first_name'],
            'account_id' => $account,
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'password'  => bcrypt($request['password']),

            'is_registered' => 1,
            'is_confirmed' => 1,
            'is_parent' => 1,
            'confirmation_code' => 11111,
        ]);

        return redirect('/account/'.$account.'/users')->with('success','added successfully');
    }

    public function edit($user)
    {
        $user = SuperUser::findOrFail($user);
        return view('users.edit', compact('user'));
    }

    public function update($user,UserEdit $request)
    {

        $user = SuperUser::findOrFail($user);
        $user->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'is_registered' => 1,
            'is_confirmed' => 1,
            'is_parent' => 1,
            'confirmation_code' => 11111,
        ]);

        if ($request['password'])
        {
            $user->update([
                'password' => bcrypt($request['password'])
            ]);
        }

        return redirect('/users/'.$user->id.'/edit')->with('status','updated successfully');
    }


    public function confirm_delete($user)
    {
        $user = SuperUser::findOrFail($user);
        return view('users.confirm',compact('user'));
    }


    public function delete($user)
    {
        $user = SuperUser::find($user);
        if ($user)
        {
            $user->delete();

            return redirect('/account/'.$user->account_id.'/users')->with('status','deleted successfully');
        }

    }



}
