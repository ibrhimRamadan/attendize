<?php

namespace App\Http\Controllers;

use App\Account;
use App\DataTables\OrganizerDataTable;
use App\Http\Requests\Organizer as OrganizerReq;
use App\Http\Requests\OrganizerEdit;
use App\Organizer;
use Illuminate\Http\Request;

class OrganizerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index($account, OrganizerDataTable $dataTable)
    {
        $account = Account::findOrFail($account);
        return $dataTable->with('account_id', $account->id)->render('organizers.index',compact('account'));
    }

    public function create($account)
    {
        $account = Account::findOrFail($account);
        return view('organizers.create',compact('account'));
    }

    public function store($account,OrganizerReq $request)
    {


        $account = Account::find($account);
        if ($account)
        {
            Organizer::create([
                'name'  => $request->name,
                'account_id'  => $account->id,
                'email'  => $request->email,
                'phone'  => $request->phone,
                'about' => '',
                'confirmation_key' => 'LzCTYJRO61EzOud',
                'is_email_confirmed' => 1,
                'show_twitter_widget' => 1,
                'show_facebook_widget' => 1,
                'page_header_bg_color' => '#ee44ee',
                'page_bg_color' => '#333',
                'page_text_color' => '#ddd',
                'enable_organiser_page' => 1,
                'tax_value' => 0,
                'charge_tax' => 0,
                'facebook' => '',
                'twitter' => '',
                'tax_name' => '',
                'tax_id' => '',

            ]);

            return redirect('/account/'.$account->id.'/organizers')->with('success','added successfully');
        }
    }



    public function edit($organizer)
    {
        $organizer = Organizer::findOrFail($organizer);
        return view('organizers.edit', compact('organizer'));
    }



    public function update($organizer,OrganizerEdit $request)
    {

        $organizer = Organizer::findOrFail($organizer);

        $organizer->update([
            'name'  => $request->name,
            'email'  => $request->email,
            'phone'  => $request->phone,
            'about' => '',
            'confirmation_key' => 'LzCTYJRO61EzOud',
            'is_email_confirmed' => 1,
            'show_twitter_widget' => 1,
            'show_facebook_widget' => 1,
            'page_header_bg_color' => '#ee44ee',
            'page_bg_color' => '#333',
            'page_text_color' => '#ddd',
            'enable_organiser_page' => 1,
            'tax_value' => 0,
            'charge_tax' => 0,
            'facebook' => '',
            'twitter' => '',
            'tax_name' => '',
            'tax_id' => '',
        ]);


        return redirect('/organizers/'.$organizer->id.'/edit')->with('status','updated successfully');
    }


    public function confirm_delete($organizer)
    {
        $organizer = Organizer::findOrFail($organizer);
        return view('organizers.confirm',compact('organizer'));
    }

    public function delete($organizer)
    {
        $organizer = Organizer::find($organizer);
        if ($organizer)
        {
            $organizer->delete();

            return redirect('/account/'.$organizer->account_id.'/organizers')->with('status','deleted successfully');
        }

    }

}
