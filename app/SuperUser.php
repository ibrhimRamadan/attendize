<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperUser extends Model
{
    protected $connection = 'mysql2';
    protected $table = "users";
    protected  $fillable = ['first_name','last_name','email','password','account_id','phone','confirmation_code','is_parent','is_confirmed','is_registered'];
}
