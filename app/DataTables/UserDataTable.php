<?php

namespace App\DataTables;

use App\SuperUser;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{



    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'users.actions')
            ->rawColumns(['action']);
    }




    public function query(SuperUser $model)
    {
        return $model->newQuery()->select('id', 'first_name','last_name','phone', 'email')->where('account_id',$this->account_id);
    }




    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'phone',
            'email',
            'action' => ['name' => 'action','title' => 'actions', 'exportable' => false, 'printable'  => false, 'searchable' => false, 'orderable'  => false],
        ];
    }



    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
