<?php

namespace App\DataTables;

use App\Organizer;
use Yajra\DataTables\Services\DataTable;

class OrganizerDataTable extends DataTable
{



    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'organizers.actions')
            ->rawColumns(['action']);
    }





    public function query(Organizer $model)
    {
        return $model->newQuery()->select('id', 'name', 'email', 'phone')->where('account_id',$this->account_id);
    }




    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            'id',
            'name',
            'email',
            'phone',
            'action' => ['name' => 'action','title' => 'actions', 'exportable' => false, 'printable'  => false, 'searchable' => false, 'orderable'  => false],
        ];
    }




    protected function filename()
    {
        return 'Organizer_' . date('YmdHis');
    }
}
