<?php

namespace App\DataTables;

use App\Account;
use Yajra\DataTables\Services\DataTable;

class AccountDataTable extends DataTable
{


    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('action', 'accounts.actions')
            ->rawColumns(['action']);

    }


    public function query(Account $model)
    {
        return $model->newQuery()->select('id', 'first_name','last_name','email','city');
    }


    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
                    ]);
    }



    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id','data' => 'id','title' => 'id'],
            'first_name' => ['title' => 'first name'],
            'last_name' => ['title' => 'last name'],
            'email' => ['title' => 'E-mail'],
            'city' => ['title' => 'city'],
            'action' => ['name' => 'action','title' => 'actions', 'exportable' => false, 'printable'  => false, 'searchable' => false, 'orderable'  => false],

        ];
    }



    protected function filename()
    {
        return 'Account_' . date('YmdHis');
    }
}
