<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $connection = 'mysql2';
    protected $table = "organisers";
    protected  $fillable = ['charge_tax','tax_id','phone','tax_value','tax_name','enable_organiser_page','page_text_color','page_bg_color','page_header_bg_color','show_facebook_widget',
        'show_twitter_widget','is_email_confirmed','twitter','facebook','confirmation_key','email','about','name','account_id'];
}
