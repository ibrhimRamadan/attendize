<?php

namespace App\Listeners;

use App\Events\AccountCreated;
use App\Organizer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateAssociatedOrganizer
{


    public function __construct()
    {
        //
    }


    public function handle(AccountCreated $event)
    {
        Organizer::create([
            'name'  => $event->account->name,
            'account_id'  => $event->account->id,
            'email'  => $event->account->email,
            'phone'  => $event->account->phone,
            'about' => '',
            'confirmation_key' => 'LzCTYJRO61EzOud',
            'is_email_confirmed' => 1,
            'show_twitter_widget' => 1,
            'show_facebook_widget' => 1,
            'page_header_bg_color' => '#ee44ee',
            'page_bg_color' => '#333',
            'page_text_color' => '#ddd',
            'enable_organiser_page' => 1,
            'tax_value' => 0,
            'charge_tax' => 0,
            'facebook' => '',
            'twitter' => '',
            'tax_name' => '',
            'tax_id' => '',


        ]);
    }
}
