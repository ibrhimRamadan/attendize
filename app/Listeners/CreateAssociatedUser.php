<?php

namespace App\Listeners;

use App\Events\AccountCreated;
use App\SuperUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateAssociatedUser
{


    public function __construct()
    {
        //
    }


    public function handle(AccountCreated $event)
    {
        # envent --> account object

        SuperUser::create([
            'first_name'  => $event->account->first_name,
            'account_id'  => $event->account->id,
            'last_name'  => $event->account->last_name,
            'email'  => $event->account->email,
            'phone'  => $event->account->phone,
            'password'  => $event->account->password,
            'is_registered' => 1,
            'is_confirmed' => 1,
            'is_parent' => 1,
            'confirmation_code' => 11111,
        ]);

    }
}
