<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $connection = 'mysql2';
    protected $table = "accounts";
    protected  $fillable = ['name','first_name','last_name','email','city','address1','address2','timezone_id','currency_id'];


    public static function add($request)
    {
       self::create([
          'name' => $request['name'],
          'first_name' => $request['first_name'],
          'last_name' => $request['last_name'],
          'city' => $request['city'],
          'address1' => $request['address1'],
          'address2' => $request['address2'],
          'email' => $request['email'],
       ]);
    }


}
